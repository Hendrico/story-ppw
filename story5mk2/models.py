from django.db import models
from django.shortcuts import reverse


semester_choices = [
    ("Gasal 2019/2020","Gasal 2019/2020"),
    ("Genap 2019/2020","Genap 2019/2020"),
    ("Gasal 2020/2021","Gasal 2020/2021"),
    ("Genap 2020/2021","Genap 2020/2021"),
    ("Gasal 2021/2022","Gasal 2021/2022"),
    ("Genap 2021/2022","Genap 2021/2022")
]

class MataKuliah(models.Model):
    nama = models.TextField(max_length = 50)
    dosen = models.TextField(max_length = 50)
    jumlahSks = models.IntegerField()
    deskripsi = models.TextField(max_length = 500)
    semester = models.TextField(choices = semester_choices)
    ruangan = models.TextField(max_length = 50)

    def __str__(self):
        return "{} ({})".format(self.nama,self.dosen)

    def get_absolute_url(self):
        return reverse("story5mk2:detailMatkul",args=[self.id])

