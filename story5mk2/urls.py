from django.urls import path
from . import views

app_name = 'story5mk2'

urlpatterns = [
    path('', views.siakenji, name='siakenji'),
    path('addMatkul/', views.addMatkul, name='addMatkul'),
    path("matkul/<int:id>",views.detailMatkul,name="detailMatkul"),
    path("deleteMatkul/<int:id>",views.deleteMatkul,name="deleteMatkul")
]