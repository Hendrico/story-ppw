from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import MatkulForm
from .models import MataKuliah

# Create your views here.
def siakenji(request):
    context = {
        "listMataKuliah" : MataKuliah.objects.all().order_by("-id"),
    }
    return render(request, 'siakenji_view.html', context)

def addMatkul(request):
    if request.method == 'POST':
        matkulForm = MatkulForm(request.POST)
        if matkulForm.is_valid():
            matkulForm.save()
        return redirect("story5mk2:siakenji")
    else:
        return render(request,"addMatkul_view.html")

def detailMatkul(request,id):
    matkul = MataKuliah.objects.get(id=id)
    context = {
        "mataKuliah" : matkul
    }
    return render(request,"detailMatkul_view.html",context)

def deleteMatkul(request,id):
    if request.method == "POST":
        mataKuliah = MataKuliah.objects.get(id=id)
        mataKuliah.delete()
    return redirect("story5mk2:siakenji")

