from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.bio, name='bio'),
    path('skill/', views.skill, name='skill'),
    path('views/', views.portofolio, name='portofolio'),
    path('history/', views.history, name='history'),
    path('contact/', views.contact, name='contact'),
    
]