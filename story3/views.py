from django.shortcuts import render

# Create your views here.
def bio(request):
    return render(request, 'bio_view.html')

def contact(request):
    return render(request, 'contact_view.html')

def history(request):
    return render(request, 'history_view.html')

def skill(request):
    return render(request, 'skill_view.html')

def portofolio(request):
    return render(request, 'portofolio_view.html')
